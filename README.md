# Project Euler
This is a repo to store my solutions to [Project Euler](https://projecteuler.net/about) problems. I've been working
through them in order for a while, whenever I get the chance and I'm feeling
bored. Many of my solutions are sub-optimal, but all solutions obey the "1-minute rule."
(95% take less than a second, and the rest are pretty much in the 1-20 second range).
All solutions are writtien in Java, and I reuse a lot of snippets (for primality testing,
GCD, sorting, etc).

I have no friends... Add me!

Project Euler Friend Key: 828335_247ff2ce1fbeefab8bb444e1f2b414f5

![](https://projecteuler.net/profile/Dextt.png)